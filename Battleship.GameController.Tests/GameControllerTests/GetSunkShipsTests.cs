﻿namespace Battleship.GameController.Tests.GameControllerTests
{
    using System.Collections.Generic;

    using Battleship.GameController.Contracts;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// The is tests for GetSunkShips method
    /// </summary>
    [TestClass]
    public class GetSunkShipsTests
    {
        private List<Ship> fleet;

        [TestInitialize]
        public void Setup()
        {
            fleet = new List<Ship>(){
                new Ship() { Name = "Destroyer", Size = 3 },
                new Ship() { Name = "Patrol Boat", Size = 2 }
            };

            fleet[0].Positions.Add(new Position { Column = Letters.B, Row = 4 });
            fleet[0].Positions.Add(new Position { Column = Letters.B, Row = 5 });
            fleet[0].Positions.Add(new Position { Column = Letters.B, Row = 6 });

            fleet[1].Positions.Add(new Position { Column = Letters.E, Row = 6 });
            fleet[1].Positions.Add(new Position { Column = Letters.E, Row = 7 });
        }

        /// <summary>
        /// Return no sunk shinks in fleet
        /// </summary>
        [TestMethod]
        public void GetSunkShips_None()
        {
            var result = GameController.GetSunkShips(fleet);

            Assert.AreEqual(0, result.Count);
        }

        /// <summary>
        /// Return a single shunk ship in fleet
        /// </summary>
        [TestMethod]
        public void GetSunkShips_Single()
        {
            fleet[1].Positions[0].IsHit = true;
            fleet[1].Positions[1].IsHit = true;

            var result = GameController.GetSunkShips(fleet);

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Patrol Boat", result[0]);
        }

        /// <summary>
        /// Return multiple sunk ships in fleet
        /// </summary>
        [TestMethod]
        public void GetSunkShips_Multiple()
        {
            fleet[0].Positions[0].IsHit = true;
            fleet[0].Positions[1].IsHit = true;
            fleet[0].Positions[2].IsHit = true;

            fleet[1].Positions[0].IsHit = true;
            fleet[1].Positions[1].IsHit = true;

            var result = GameController.GetSunkShips(fleet);

            Assert.AreEqual(2, result.Count);
            Assert.AreEqual("Destroyer", result[0]);
            Assert.AreEqual("Patrol Boat", result[1]);
        }
    }
}