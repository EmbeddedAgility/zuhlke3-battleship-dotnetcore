﻿namespace Battleship.GameController.Tests.GameControllerTests
{
    using System;
    using System.Collections.Generic;

    using Battleship.GameController.Contracts;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// The is tests for IsShipSunk method
    /// </summary>
    [TestClass]
    public class IsShipSunkTests
    {
        private List<Ship> fleet;

        [TestInitialize]
        public void Setup()
        {
            fleet = new List<Ship>(){
                new Ship() { Name = "Patrol Boat", Size = 2 }
            };

            fleet[0].Positions.Add(new Position { Column = Letters.E, Row = 6 });
            fleet[0].Positions.Add(new Position { Column = Letters.E, Row = 7 });
        }

        /// <summary>
        /// The ship is not valid.
        /// </summary>
        [TestMethod]
        public void IsShipSunk_True()
        {
            fleet[0].Positions[0].IsHit = true;
            fleet[0].Positions[1].IsHit = true;

            var result = GameController.IsShipSunk(fleet, "Patrol Boat");

            Assert.IsTrue(result);
        }

        /// <summary>
        /// The ship is valid.
        /// </summary>
        [TestMethod]
        public void IsShipSunk_False()
        {
            bool result = GameController.IsShipSunk(fleet, "Patrol Boat");

            Assert.IsFalse(result);
        }

        /// <summary>
        /// The ship is valid.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(Exception), "No ship found with name Invalid Name")]
        public void IsShipSunk_Invalid()
        {
            var result = GameController.IsShipSunk(fleet, "Invalid Name");
        }
    }
}