﻿namespace Battleship.GameController.Tests.GameControllerTests
{
    using System.Collections.Generic;

    using Battleship.GameController.Contracts;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// The is tests for AllShipsSunk method
    /// </summary>
    [TestClass]
    public class AllShipsSunkTests
    {
        private List<Ship> fleet;

        [TestInitialize]
        public void Setup()
        {
            fleet = new List<Ship>(){
                new Ship() { Name = "Destroyer", Size = 3 },
                new Ship() { Name = "Patrol Boat", Size = 2 }
            };

            fleet[0].Positions.Add(new Position { Column = Letters.B, Row = 4 });
            fleet[0].Positions.Add(new Position { Column = Letters.B, Row = 5 });
            fleet[0].Positions.Add(new Position { Column = Letters.B, Row = 6 });

            fleet[1].Positions.Add(new Position { Column = Letters.E, Row = 6 });
            fleet[1].Positions.Add(new Position { Column = Letters.E, Row = 7 });
        }


        /// <summary>
        /// Tests that AllShipsSunk returns false if no ship is sunk
        /// </summary>
        [TestMethod]
        public void AllShipsSunk_NoSunkShip_False()
        {
            var result = GameController.AllShipsSunk(fleet);

            Assert.IsFalse(result);
        }

        /// <summary>
        /// Tests that AllShipsSunk returns false if not all ships are sunk
        /// </summary>
        [TestMethod]
        public void AllShipsSunk_SingleSunkShip_False()
        {
            fleet[1].Positions[0].IsHit = true;
            fleet[1].Positions[1].IsHit = true;

            var result = GameController.AllShipsSunk(fleet);

            Assert.IsFalse(result);
        }

        /// <summary>
        /// Tests that AllShipsSunk returns true if all ships are sunk
        /// </summary>
        [TestMethod]
        public void AllShipsSunk_AllSunkShip_True()
        {
            fleet[0].Positions[0].IsHit = true;
            fleet[0].Positions[1].IsHit = true;
            fleet[0].Positions[2].IsHit = true;

            fleet[1].Positions[0].IsHit = true;
            fleet[1].Positions[1].IsHit = true;

            var result = GameController.AllShipsSunk(fleet);

            Assert.IsTrue(result);
        }
    }
}