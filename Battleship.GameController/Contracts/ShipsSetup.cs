﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Battleship.GameController.Contracts
{
    public class ShipsSetup
    {
        public List<List<Ship>> setup { get; set; }

        public ShipsSetup()
        {
            setup = new List<List<Ship>>();
            var fleet = GameController.InitializeShips().ToList();
            fleet[0].Positions.Add(new Position { Column = Letters.B, Row = 4 });
            fleet[0].Positions.Add(new Position { Column = Letters.B, Row = 5 });
            fleet[0].Positions.Add(new Position { Column = Letters.B, Row = 6 });
            fleet[0].Positions.Add(new Position { Column = Letters.B, Row = 7 });
            fleet[0].Positions.Add(new Position { Column = Letters.B, Row = 8 });

            fleet[1].Positions.Add(new Position { Column = Letters.E, Row = 5 });
            fleet[1].Positions.Add(new Position { Column = Letters.E, Row = 6 });
            fleet[1].Positions.Add(new Position { Column = Letters.E, Row = 7 });
            fleet[1].Positions.Add(new Position { Column = Letters.E, Row = 8 });

            fleet[2].Positions.Add(new Position { Column = Letters.A, Row = 3 });
            fleet[2].Positions.Add(new Position { Column = Letters.B, Row = 3 });
            fleet[2].Positions.Add(new Position { Column = Letters.C, Row = 3 });

            fleet[3].Positions.Add(new Position { Column = Letters.F, Row = 8 });
            fleet[3].Positions.Add(new Position { Column = Letters.G, Row = 8 });
            fleet[3].Positions.Add(new Position { Column = Letters.H, Row = 8 });

            fleet[4].Positions.Add(new Position { Column = Letters.C, Row = 5 });
            fleet[4].Positions.Add(new Position { Column = Letters.C, Row = 6 });
            setup.Add(fleet);

            fleet = GameController.InitializeShips().ToList();
            fleet[0].Positions.Add(new Position { Column = Letters.B, Row = 4 });
            fleet[0].Positions.Add(new Position { Column = Letters.C, Row = 4 });
            fleet[0].Positions.Add(new Position { Column = Letters.D, Row = 4 });
            fleet[0].Positions.Add(new Position { Column = Letters.E, Row = 4 });
            fleet[0].Positions.Add(new Position { Column = Letters.F, Row = 4 });

            fleet[1].Positions.Add(new Position { Column = Letters.A, Row = 5 });
            fleet[1].Positions.Add(new Position { Column = Letters.A, Row = 6 });
            fleet[1].Positions.Add(new Position { Column = Letters.A, Row = 7 });
            fleet[1].Positions.Add(new Position { Column = Letters.A, Row = 8 });

            fleet[2].Positions.Add(new Position { Column = Letters.H, Row = 1 });
            fleet[2].Positions.Add(new Position { Column = Letters.H, Row = 2 });
            fleet[2].Positions.Add(new Position { Column = Letters.H, Row = 3 });

            fleet[3].Positions.Add(new Position { Column = Letters.B, Row = 6 });
            fleet[3].Positions.Add(new Position { Column = Letters.B, Row = 7 });
            fleet[3].Positions.Add(new Position { Column = Letters.B, Row = 8 });

            fleet[4].Positions.Add(new Position { Column = Letters.C, Row = 2 });
            fleet[4].Positions.Add(new Position { Column = Letters.D, Row = 2 });
            setup.Add(fleet);


            fleet = GameController.InitializeShips().ToList();
            fleet[0].Positions.Add(new Position { Column = Letters.A, Row = 8 });
            fleet[0].Positions.Add(new Position { Column = Letters.B, Row = 8 });
            fleet[0].Positions.Add(new Position { Column = Letters.C, Row = 8 });
            fleet[0].Positions.Add(new Position { Column = Letters.D, Row = 8 });
            fleet[0].Positions.Add(new Position { Column = Letters.E, Row = 8 });

            fleet[1].Positions.Add(new Position { Column = Letters.D, Row = 1 });
            fleet[1].Positions.Add(new Position { Column = Letters.E, Row = 1 });
            fleet[1].Positions.Add(new Position { Column = Letters.F, Row = 1 });
            fleet[1].Positions.Add(new Position { Column = Letters.G, Row = 1 });

            fleet[2].Positions.Add(new Position { Column = Letters.C, Row = 3 });
            fleet[2].Positions.Add(new Position { Column = Letters.C, Row = 3 });
            fleet[2].Positions.Add(new Position { Column = Letters.C, Row = 3 });

            fleet[3].Positions.Add(new Position { Column = Letters.G, Row = 4 });
            fleet[3].Positions.Add(new Position { Column = Letters.G, Row = 5 });
            fleet[3].Positions.Add(new Position { Column = Letters.G, Row = 6 });

            fleet[4].Positions.Add(new Position { Column = Letters.A, Row = 2 });
            fleet[4].Positions.Add(new Position { Column = Letters.A, Row = 3 });
            setup.Add(fleet);


            fleet = GameController.InitializeShips().ToList();
            fleet[0].Positions.Add(new Position { Column = Letters.F, Row = 3 });
            fleet[0].Positions.Add(new Position { Column = Letters.F, Row = 4 });
            fleet[0].Positions.Add(new Position { Column = Letters.F, Row = 5 });
            fleet[0].Positions.Add(new Position { Column = Letters.F, Row = 6 });
            fleet[0].Positions.Add(new Position { Column = Letters.F, Row = 7 });

            fleet[1].Positions.Add(new Position { Column = Letters.A, Row = 3 });
            fleet[1].Positions.Add(new Position { Column = Letters.B, Row = 3 });
            fleet[1].Positions.Add(new Position { Column = Letters.C, Row = 3 });
            fleet[1].Positions.Add(new Position { Column = Letters.D, Row = 3 });

            fleet[2].Positions.Add(new Position { Column = Letters.B, Row = 7 });
            fleet[2].Positions.Add(new Position { Column = Letters.C, Row = 7 });
            fleet[2].Positions.Add(new Position { Column = Letters.D, Row = 7 });

            fleet[3].Positions.Add(new Position { Column = Letters.A, Row = 5 });
            fleet[3].Positions.Add(new Position { Column = Letters.A, Row = 6 });
            fleet[3].Positions.Add(new Position { Column = Letters.A, Row = 7 });

            fleet[4].Positions.Add(new Position { Column = Letters.D, Row = 2 });
            fleet[4].Positions.Add(new Position { Column = Letters.E, Row = 2 });
            setup.Add(fleet);
        }
    }
}
