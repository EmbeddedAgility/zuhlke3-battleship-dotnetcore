﻿
namespace Battleship.Ascii.Tests
{
    using Battleship.GameController.Contracts;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;

    [TestClass]
    public class ParsePositionTests
    {
        [TestMethod]
        public void ParsePosition_Valid()
        {
            var actual = Program.ParsePosition("A1");
            var expected = new Position(Letters.A, 1);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Invalid position")]
        public void ParsePosition_InvalidLetter_ThrowException()
        {
            var actual = Program.ParsePosition("Z1");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Invalid position")]
        public void ParsePosition_InvalidNumber_ThrowException()
        {
            var actual = Program.ParsePosition("A9999");
        }
    }
}
