﻿
namespace Battleship.Ascii
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Media;
    using Battleship.GameController;
    using Battleship.GameController.Contracts;

    public class Program
    {
        private static List<Ship> myFleet;

        private static List<Ship> enemyFleet;
        private static int length = 8;

        private static string secretBackdoorWin = "#supersecretbackdoorwin";
        private static string secretBackdoorLose = "#supersecretbackdoorlose";


        static void Main()
        {
            Console.Title = "Battleship";
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Clear();

            Console.WriteLine("                                     |__");
            Console.WriteLine(@"                                     |\/");
            Console.WriteLine("                                     ---");
            Console.WriteLine("                                     / | [");
            Console.WriteLine("                              !      | |||");
            Console.WriteLine("                            _/|     _/|-++'");
            Console.WriteLine("                        +  +--|    |--|--|_ |-");
            Console.WriteLine(@"                     { /|__|  |/\__|  |--- |||__/");
            Console.WriteLine(@"                    +---------------___[}-_===_.'____                 /\");
            Console.WriteLine(@"                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _");
            Console.WriteLine(@" __..._____--==/___]_|__|_____________________________[___\==--____,------' .7");
            Console.WriteLine(@"|                        Welcome to Battleship                         BB-61/");
            Console.WriteLine(@" \_________________________________________________________________________|");
            Console.WriteLine();

            InitializeGame();

            StartGame();
        }

        private static void HitTarget(ConsoleColor color, IEnumerable<Ship> fleet, Ship hitShip)
        {
            Console.ForegroundColor = color;
            Console.Beep();

            Console.WriteLine(@"                \         .  ./");
            Console.WriteLine(@"              \      .:"";'.:..""   /");
            Console.WriteLine(@"                  (M^^.^~~:.'"").");
            Console.WriteLine(@"            -   (/  .    . . \ \)  -");
            Console.WriteLine(@"               ((| :. ~ ^  :. .|))");
            Console.WriteLine(@"            -   (\- |  \ /  |  /)  -");
            Console.WriteLine(@"                 -\  \     /  /-");
            Console.WriteLine(@"                   \  \   /  /");
            Console.WriteLine();

            PrintShipSunk(fleet, hitShip.Name, color);

            Console.WriteLine();
        }

        private static void MissTarget()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Beep();

            Console.WriteLine(@"               _      _      _      _      _ ");
            Console.WriteLine(@"               )`'-.,_)`'-.,_)`'-.,_)`'-.,_)");
            Console.WriteLine(@"               _      _      _      _   ");
            Console.WriteLine(@"                  )`'-.,_)`'-.,_)`'-.,_)");
            Console.WriteLine(@"                    _      _      _    ");
            Console.WriteLine(@"                     )`'-.,_)`'-.,_)");
            Console.WriteLine(@"                     _      _      _  ");
            Console.WriteLine(@"                    )`'-.,_)`'-.,_)");
            Console.WriteLine();
        }

        private static void StartGame()
        {
            bool backdoorWinActivated = false;
            bool backdoorLoseActivated = false;

            Console.Clear();
            Console.WriteLine("                  __");
            Console.WriteLine(@"                 /  \");
            Console.WriteLine("           .-.  |    |");
            Console.WriteLine(@"   *    _.-'  \  \__/");
            Console.WriteLine(@"    \.-'       \");
            Console.WriteLine("   /          _/");
            Console.WriteLine(@"  |      _  /""");
            Console.WriteLine(@"  |     /_\'");
            Console.WriteLine(@"   \    \_/");
            Console.WriteLine(@"    """"""""");

            do
            {
                Console.WriteLine();
                Console.WriteLine("------------------------------------");
                Console.WriteLine("Player, it's your turn");
                List<string> enemySunkShips = GameController.GetSunkShips(enemyFleet);
                string enemySunkShipsMessage = enemySunkShips.Any() ? string.Join(", ", enemySunkShips) : "none";
                Console.WriteLine("Enemy sunk ships: " + enemySunkShipsMessage);
                Console.ForegroundColor = ConsoleColor.Yellow;

                Console.WriteLine("Possible positions are combination of letters from A to {0} and numbers from 1-{1}", Letters.H, length);
                Console.ForegroundColor = ConsoleColor.White;

                
                Position position = null;
                bool stopGame = false;
                do
                {
                    Console.WriteLine("Enter coordinates for your shot :");
                    try
                    {
                        string input = Console.ReadLine();
                        backdoorWinActivated = input == secretBackdoorWin;
                        backdoorLoseActivated = input == secretBackdoorLose;
                        if (backdoorWinActivated || backdoorLoseActivated)
                        {
                            stopGame = true;
                            break;
                        }
                        position = ParsePosition(input);
                    }
                    catch (Exception)
                    {
                        
                    }

                } while (position == null);

                if (stopGame)
                {
                    break;
                }
                Ship hitShip = null;
                var isHit = GameController.CheckIsHit(enemyFleet, position, out hitShip);
                if (isHit)
                {
                    HitTarget(ConsoleColor.Green, enemyFleet, hitShip);
                }
                else
                {
                    MissTarget();
                    Console.ForegroundColor = ConsoleColor.Red;
                }


                Console.WriteLine(isHit ? "Yeah ! Nice hit !" : "Miss");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine();

                if (GameController.AllShipsSunk(enemyFleet))
                {
                    break;
                }

                Console.WriteLine("------------------------------------");
                Console.WriteLine("It's the enemy's turn!");
                isHit = GameController.CheckIsHit(myFleet, position, out hitShip);
                List<string> mySunkShips = GameController.GetSunkShips(myFleet);
                string mySunkShipsMessage = mySunkShips.Any() ? string.Join(", ", mySunkShips) : "none";
                Console.WriteLine();
                Console.WriteLine("My sunk ships: " + mySunkShipsMessage);

                position = GetRandomPosition();
                isHit = GameController.CheckIsHit(myFleet, position, out hitShip);
                Console.WriteLine();
                if (isHit)
                {
                    HitTarget(ConsoleColor.Red, myFleet, hitShip);
                }
                else
                {
                    MissTarget();
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                Console.WriteLine("Computer shot in {0}{1} and {2}", position.Column, position.Row, isHit ? "has hit your ship !" : "miss");
                Console.ForegroundColor = ConsoleColor.White;
                if (GameController.AllShipsSunk(myFleet))
                {
                    break;
                }
            }
            while (true);

            
            if (backdoorLoseActivated || GameController.AllShipsSunk(myFleet))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine();
                Console.WriteLine();
                string text = @"
                ............******..........................................
                .............*****..........................................
                ............................................................
                ............................................................
                .......***..................................................
                .......****.................................................
                .......***. ................................................
                ......****.   ..............................................
                .........*.     ............................................
                .........*.       ............................***********.. 
                ..........*        ...........................********..    
                ..........*.         .........................******..      
                ............          .........................***.         
                ............           ..........................           
                .............            ......................             
                ............*.            ...................              .
                ............*.            .....           .                *
                ............**.                                           .*
                ...........****.                                         ...
                ...........*****.                                        ...
                ........*********.  .                                   ....
                ..****************...                                 ......
                ..*****************.                              .. .......
                .*****************.                                .........
                .*****************.                                 ........
                .****************.      ...               ...       ........
                ****************..     . .*.             . .*        .......
                ***************.*.     . .*.             . .*.       .......
                **************...      .***.             ****.       .*.....
                **************...      .***              .**.         ...***
                **************.*.       ..                ..          .*****
                **************..               ...                    .*****
                **************..  ..           ....            ..      *****
                *************.........                        .....    .****
                ************..........                       .......   .....
                **********.............          ...         .......   .....
                *.............. ......         ......        .......    ....
                ................ .....        ........       .......    ....
                ................              ........        .....     ....
                ................              ........                  ....
                ................               .......                   ...
                .................               .....                    ...
                .................                                        ...
                ..................                                       ...
                ..................                                       ...
                ...................                                      ...
                ...................                                       ..
                ...................                                       ..
                .................*                                        ..
                ";
                Console.WriteLine(text);
                PlaySound("\\Sounds\\nooo.wav");
                Console.ForegroundColor = ConsoleColor.White;
            }
            else if (backdoorWinActivated || GameController.AllShipsSunk(enemyFleet))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine();
                Console.WriteLine();
                string text = @"
                     __     ______  _    _   _____   ____   _____ _  __  __  __          _   _ _ 
                     \ \   / / __ \| |  | | |  __ \ / __ \ / ____| |/ / |  \/  |   /\   | \ | | |
                      \ \_/ / |  | | |  | | | |__) | |  | | |    | ' /  | \  / |  /  \  |  \| | |
                       \   /| |  | | |  | | |  _  /| |  | | |    |  <   | |\/| | / /\ \ | . ` | |
                        | | | |__| | |__| | | | \ \| |__| | |____| . \  | |  | |/ ____ \| |\  |_|
                        |_|  \____/ \____/  |_|  \_\\____/ \_____|_|\_\ |_|  |_/_/    \_\_| \_(_)
                ";
                Console.WriteLine(text);
                Console.WriteLine();
                Console.WriteLine();
                PlaySound("\\Sounds\\WeAreTheChampions.wav");
                Console.ForegroundColor = ConsoleColor.White;
            }

            Console.ReadKey();
        }

        public static Position ParsePosition(string input)
        {
            var valid = Enum.TryParse<Letters>(input.ToUpper().Substring(0, 1), true, out var letter);
            if (valid)
            {
                valid = ValidatePosition((int)letter + 1);
            }
            else
            {
                throw new Exception("Invalid position");
            }
            var number = int.Parse(input.Substring(1));
            valid = ValidatePosition(number);
            if (valid)
            {
                return new Position((Letters)letter, number);
            }
            else
            {
                throw new Exception("Invalid position");
            }
        }

        public static bool ValidatePosition(int position)
        {
            if (position < length)
            {
                return true;
            }
            return false;
        }

        private static Position GetRandomPosition()
        {
            int rows = length;
            int lines = length;
            var random = new Random();
            var letter = (Letters)random.Next(lines);
            var number = random.Next(rows);
            var position = new Position(letter, number);
            return position;
        }

        private static void InitializeGame()
        {
            InitializeMyFleet();

            InitializeEnemyFleet();
        }

        private static void InitializeMyFleet()
        {
            myFleet = GameController.InitializeShips().ToList();

            Console.WriteLine("Please position your fleet (Game board size is from A to H and 1 to 8) :");

            foreach (var ship in myFleet)
            {
                Console.WriteLine();
                Console.WriteLine("Please enter the positions for the {0} (size: {1})", ship.Name, ship.Size);
                for (var i = 1; i <= ship.Size; i++)
                {
                    Console.WriteLine("Enter position {0} of {1} (i.e A3):", i, ship.Size);
                    ship.AddPosition(Console.ReadLine());
                }
            }
        }

        private static void InitializeEnemyFleet()
        {
            enemyFleet = GameController.InitializeShips().ToList();

            var random = new Random();
            var setup = new ShipsSetup();
            var number = random.Next(0, 3);
            enemyFleet = setup.setup.GetRange(number, 1).First();
        }

        private static void PrintShipSunk(IEnumerable<Ship> fleet, string hitShipName, ConsoleColor color)
        {
            if (hitShipName != null)
            {
                bool isShipSunk = GameController.IsShipSunk(fleet, hitShipName);
                if (isShipSunk)
                {
                    Console.ForegroundColor = color;
                    Console.WriteLine("Ship " + hitShipName + " has sunk!");
                }
            }
        }

        private static void PlaySound(string relativePath)
        {
            SoundPlayer player = new SoundPlayer();
            player.SoundLocation = AppDomain.CurrentDomain.BaseDirectory + relativePath;
            player.Play();
        }

        private static void MyenemyFleet(List<Ship> enemyFleet)
        {
            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 4 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 5 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 6 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 7 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 8 });

            enemyFleet[1].Positions.Add(new Position { Column = Letters.E, Row = 6 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.E, Row = 7 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.E, Row = 8 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.E, Row = 9 });

            enemyFleet[2].Positions.Add(new Position { Column = Letters.A, Row = 3 });
            enemyFleet[2].Positions.Add(new Position { Column = Letters.B, Row = 3 });
            enemyFleet[2].Positions.Add(new Position { Column = Letters.C, Row = 3 });

            enemyFleet[3].Positions.Add(new Position { Column = Letters.F, Row = 8 });
            enemyFleet[3].Positions.Add(new Position { Column = Letters.G, Row = 8 });
            enemyFleet[3].Positions.Add(new Position { Column = Letters.H, Row = 8 });

            enemyFleet[4].Positions.Add(new Position { Column = Letters.C, Row = 5 });
            enemyFleet[4].Positions.Add(new Position { Column = Letters.C, Row = 6 });
        }
    }
}
